<?php

namespace App;

use App\Traits\ModelUuidTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, ModelUuidTrait;

    /** @var string */
    protected $firstname;

    /** @var string */
    protected $lastname;

    /** @var string */
    protected $email;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be permanently appended to the response object
     *
     * @var array
     */
    protected $appends = [
        'full_name'
    ];

    /**
     * Permanent lazy load relationships
     *
     * @var array
     */

    protected $with = [

    ];

    /**
     * Permanent lazy load relationship totals
     *
     * @var array
     */
    protected $withCount = [

    ];

    /*
     * Accessors
     */

    /**
     * @return string
     */
    function getFullNameAttribute(): string
    {
        return sprintf('%s %s', $this->attributes['firstname'],$this->attributes['lastname']);
    }

    /*
     * Mutators
     */

    /**
     * @param $value
     */
    public function setFirstnameAttribute($value)
    {
        $this->attributes['firstname'] = ucwords(trim($value));
    }

    /**
     * @param $value
     */
    public function setLastnameAttribute($value)
    {
        $this->attributes['lastname'] = ucwords(trim($value));
    }

    /*
     * Scopes
     */

    /*
     * Relationships
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blogs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Blog::class);
    }
}
