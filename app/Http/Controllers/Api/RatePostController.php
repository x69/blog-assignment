<?php

namespace App\Http\Controllers\Api;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;

class RatePostController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function __invoke()
    {
        // validate
        $data = request()->validate([
            'post_id' => 'required|uuid|exists:blogs,id',
            'rating'  => 'required|numeric|min:1|max:5',
        ]);
        // cast vote
        $post = Blog::where('id', $data['post_id'])->firstOrFail();
        $post->ratings()->create(['rating' => $data['rating']]);
        return $post->fresh();
    }
}
