<?php

namespace App\Http\Controllers\Api;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // return response object
        return Blog::orderBy('created_at', 'desc')->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        $data = request()->validate([
            'title'     => 'required|filled|min:15|max:200',
            'blog_post' => 'required|filled|min:100',
        ]);
        try {
            // create entry
            auth()->user()->blogs()->create($data);
            // return response
            return response()->json(['success' => true, 'message' => 'success'], 201);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param string $id
     * @return Response
     */
    public function update(string $id)
    {
        // get post
        $post = Blog::where('id', $id)->firstOrFail();
        $data = request()->validate([
            'title'     => 'required|filled|min:15|max:200',
            'blog_post' => 'required|filled|min:100',
        ]);
        try {
            // update entry
            $post->update($data);
            // return response
            return response()->json(['success' => true, 'message' => 'success'], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return Response
     */
    public function destroy(string $id)
    {
        // get post
        $post = Blog::where('id', $id)->whereHas('user', function ($user) {
            $user->id = auth()->user()->id;
        })->firstOrFail();
        try {
            // delete entry
            $post->ratings()->delete();
            $post->delete();
            // return response
            return response()->json(['success' => true, 'message' => 'success'], 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], $e->getCode());
        }
    }
}
