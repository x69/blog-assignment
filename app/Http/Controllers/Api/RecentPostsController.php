<?php

namespace App\Http\Controllers\Api;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class RecentPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function __invoke()
    {
        return Blog::select('title', 'slug')->orderBy('created_at', 'desc')->limit(10)->get();
    }

}
