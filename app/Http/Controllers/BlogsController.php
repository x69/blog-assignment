<?php

namespace App\Http\Controllers;

use App\Blog;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('blog.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        $data = request()->validate([
            'title'     => 'required|filled|max:200',
            'blog_post' => 'required|filled',
        ]);
        // create entry
        auth()->user()->blogs()->create($data);
        return redirect('blog');
    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return Response
     */
    public function show(string $slug)
    {
        $post = Blog::where('slug', $slug)->firstOrFail();
        // nl2br blog post
        $post->blog_post = nl2br($post->blog_post);
        return view('blog.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        // get post
        $post = Blog::where('id', $id)->firstOrFail();
        return view('blog.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param string $id
     * @return Response
     */
    public function update(string $id)
    {
        // get post
        $post = Blog::where('id', $id)->firstOrFail();
        $data = request()->validate([
            'title'     => 'required|filled|max:200',
            'blog_post' => 'required|filled',
        ]);
        // update entry
        $post->update($data);
        return redirect('/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $id
     * @return Response
     */
    public function destroy(string $id)
    {
        // get post
        $post = Blog::where('id', $id)->firstOrFail();
        // delete entry
        $post->ratings()->delete();
        $post->delete();
        return redirect('/blog');
    }
}
