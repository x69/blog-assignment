<?php

namespace App;

use App\Traits\ModelUuidTrait;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use ModelUuidTrait;

    /** @var integer */
    protected $rating;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'blog_id', 'rating',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * The attributes that should be permanently appended to the response object
     *
     * @var array
     */
    protected $appends = [

    ];

    /**
     * Permanent lazy load relationships
     *
     * @var array
     */

    protected $with = [

    ];

    /**
     * Permanent lazy load relationship totals
     *
     * @var array
     */
    protected $withCount = [

    ];

    /*
     * Relationships
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blog(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Blog::class);
    }
}
