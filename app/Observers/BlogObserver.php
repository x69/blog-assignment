<?php

namespace App\Observers;

use App\Blog;
use Faker\Factory;
use Illuminate\Support\Str;

class BlogObserver
{
    /**
     * @param Blog $blog
     */
    public function creating(Blog $blog)
    {
        if(empty($blog->image_url)) {
            $faker = Factory::create();
            $blog->image_url = $faker->imageUrl(895,250, null, true, null, true);
        }
        $blog->slug = Blog::makeSlug($blog->title);
    }

    /**
     * @param Blog $blog
     */
    public function updating(Blog $blog)
    {
        if(empty($blog->image_url)) {
            $faker = Factory::create();
            $blog->image_url = $faker->imageUrl(895,250, null, true, null, true);
        }
        $blog->slug = Str::slug($blog->title);
    }

    /**
     * @param Blog $blog
     */
    public function saving(Blog $blog)
    {
        if(empty($blog->image_url)) {
            $faker = Factory::create();
            $blog->image_url = $faker->imageUrl(895,250, null, true, null, true);
        }
        $blog->slug = Str::slug($blog->title);
    }
}
