<?php

namespace App;

use App\Traits\ModelUuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
    use ModelUuidTrait;

    /** @var string */
    protected $title;

    /** @var string */
    protected $image_url;

    /** @var string */
    protected $blog_post;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'image_url', 'blog_post', 'slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * The attributes that should be permanently appended to the response object
     *
     * @var array
     */
    protected $appends = [
        'average_rating', 'description', 'short_title'
    ];

    /**
     * Permanent lazy load relationships
     *
     * @var array
     */

    protected $with = [
        'user'
    ];

    /**
     * Permanent lazy load relationship totals
     *
     * @var array
     */
    protected $withCount = [

    ];

    /*
     * Accessors
     */

    /**
     * @return float|int
     */
    function getAverageRatingAttribute()
    {
        if($this->ratings()->count() > 0) {
            return round(($this->ratings()->sum('rating')/$this->ratings()->count()),0);
        }

        return 0;
    }

    /**
     * @return string
     */
    function getShortTitleAttribute(): string
    {
        return Str::limit(plaintext($this->attributes['title']),55);
    }

    /**
     * @return string
     */
    function getDescriptionAttribute(): string
    {
        if(!empty($this->attributes['blog_post'])) {
            return Str::limit(plaintext($this->attributes['blog_post']),350);
        }
        return '';
    }

    /*
     * Mutators
     */


    /*
     * Scopes
     */

    /**
     * @param $query
     */
    public function scopeWithRatings($query) {
        $query->whereHas('ratings');
    }

    /*
     * Relationships
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /*
     * Functions
     */

    /**
     * @param $title
     * @param false $exists
     * @return string
     */
    public static function makeSlug($title, $exists = false): string
    {
        $slug = Str::slug($title);
        if(Blog::where('slug', $slug)->count() == 0) {
            return $slug;
        } else {
            return self::makeSlug(sprintf('%s%s', $title, mt_rand(1,100)));
        }
    }
}
