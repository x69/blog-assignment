<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\BlogsController;

Route::get('/', function () {
    return redirect('blog');
});

Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();

// soft api routes
Route::group(['middleware' => ['web'], 'prefix' => 'api', 'namespace' => 'Api'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::resource('blog', 'BlogsController')->only(['store', 'update', 'destroy']);
    });
    Route::get('blog/{slug}', 'BlogsController@show');
    Route::get('blog', 'BlogsController@index');
    Route::get('recent-posts', 'RecentPostsController');
    Route::post('rate-post', 'RatePostController');
});

// web routes
Route::group(['middleware' => ['auth', 'web']], function () {
    Route::resource('blog', 'BlogsController')->except(['index', 'show']);
});

Route::get('blog', 'BlogsController@index');
Route::get('blog/{slug}', 'BlogsController@show');
