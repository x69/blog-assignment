# Blog Test
#### Task of Frederick Van Wyk

## Installation Notes
- I have committed the compiled css and js files, no need to run `npm install` or `npm run dev`
- I have committed my environment file `.env` as reference

## Installation
- Update the environemnt file `.env` with correct database connection credentials
- One would usually run the command `composer install` - but I have included the vondor files in the zip, therefore no need to run this command.
- Run the command `php artisan db:seed`

## General Notes
- Blog images - I have added an image field to the `blogs` table, however I initially thought to add a separate `images` table with a `belongsTo` relationship to `blogs` and a `isPrimary` table field
on the `images` table, then using a scope on the `Image` model to get the primary image, however this would be beyond the task instructions so I have used a single `image_url` table field in the `blogs` table instead.
- Onc reating or editing a post, I have moved the image value to the blog observer
- WYSIWYG editor - I havnt added an HTML editor to post blog posts and using "new line to break" instead. However I can add this if needed
- Rating - I have implemented rating as a $_GET request with no validation if an IP source or user has voted before. Also, rating is open to the public.
- Routes - I like configuring my routes (prefixes, middleware etc) in the route service provider, but for the scope of the task, all route logic resides in my `web.php` routes file

## Unit Testing
- I havnt completed the unit testing part, I have run out of time. However usually I would not make use of Blade, but VueJS instead (de-coupling my view from the actual application) - thus returning the response object 
in my controllers, then using unit testing to assert if I see specific fields or then when posting or creating, asserting the response code and validation possible validation errors received from the controller.

## Login Credentials
Username: `admin@localhost.com`
Password: `password`