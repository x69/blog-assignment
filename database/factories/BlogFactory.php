<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'user_id' => (User::inRandomOrder()->first())->id,
        'title'     => $faker->sentence(mt_rand(5,10)),
        'image_url' => $faker->imageUrl(895,250, null, true, null, true), // remote faker image url - I will not be using local disk storage
        'blog_post' => $faker->paragraphs(mt_rand(2,9), true),
    ];
});
