<?php

use App\Blog;
use Illuminate\Database\Seeder;

class RatingSeeder extends Seeder
{
    /** @var string */
    private $ratingsPerRandomPost = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // I will be adding ratings to 70% of the blog posts only
        $totalBlogPosts = Blog::count();
        // get random blog posts and add ratings
        Blog::inRandomOrder()->limit(ceil($totalBlogPosts/.70))->get()->each(function (Blog $post) {
            for($i = 0; $i < $this->ratingsPerRandomPost; $i++) {
                $post->ratings()->create([
                    'rating' => mt_rand(1,5)
                ]);
            }
        });
    }
}
