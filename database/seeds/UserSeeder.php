<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create real user (update or create for if the seeder is run more than once)
        User::updateOrCreate(
            ['email' => 'admin@localhost.com'],
            [
                'firstname'         => 'Test',
                'lastname'          => 'Admin',
                'email'             => 'admin@localhost.com',
                'email_verified_at' => now(),
                'password'          => bcrypt('password'),
                'remember_token'    => Str::random(10),
            ]
        );
        // create dummy users
        factory(App\User::class, 50)->create();
    }
}
