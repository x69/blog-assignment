<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BlogTest extends TestCase
{
    /**
     * @return mixed
     */
    private function getUser() {
        return User::where('email', 'admin@localhost.com')->firstOrFail();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexTest()
    {

        $response = $this->actingAs($this->getUser())->get('/blog');

        $response->assertStatus(200);
    }
}
