@extends('layouts.app')

@section('content')
    <blog-post-show :user="{{ optional(auth()->user())->firstname ? auth()->user():'{}' }}" :post="{{ $post }}"></blog-post-show>
@endsection
