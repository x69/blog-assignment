@extends('layouts.app')

@section('content')
    <blog-posts :user="{{ optional(auth()->user())->firstname ? auth()->user():'{}' }}"></blog-posts>
@endsection
