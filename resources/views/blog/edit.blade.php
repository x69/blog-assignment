@extends('layouts.app')

@section('content')
    <blog-edit :post="{{ $post }}"></blog-edit>
@endsection
