@for($i = 1; $i <= $post->average_rating; $i++)
    <a href="rate/{{ $post->id }}/{{ $i }}" title="Give a {{ $i }} start rating"><span class="fa fa-star checked"></span></a>
@endfor
@for($i = 1; $i <= (5-$post->average_rating); $i++)
    <a href="rate/{{ $post->id }}/{{ $i }}" title="Give a {{ $post->average_rating+$i }} start rating"><span class="fa fa-star-o"></span></a>
@endfor
