/**
 * Format the given date as a timestamp.
 */
Vue.filter("datetime", value => {
    return moment().format("D MMM YYYY h:mm A");
});


/**
 * Format the given date into a relative time.
 */
Vue.filter("relative", value => {
    return moment.utc(value).local().locale("en").fromNow();
});

/**
 * Convert the first character to upper case.
 *
 * Source: https://github.com/vuejs/vue/blob/1.0/src/filters/index.js#L37
 */
Vue.filter("capitalize", value => {
    if (!value && value !== 0) {
        return "";
    }

    return value.toString().charAt(0).toUpperCase()
        + value.slice(1);
});

Vue.filter("lowercase", value => {
    if (!value && value !== 0) {
        return "";
    }

    return value.toString().charAt(0).toLowerCase()
        + value.slice(1);
});


Vue.filter("slugtotext", value => {

    if (!value) {
        return value;
    }

    value = value + '';

    return value.split(" ")
        .map(w => w[0].toUpperCase() + w.substr(1).toLowerCase())
        .join(" ").replace(/-/g, ' ');

});
