require('./bootstrap');

Vue.component('layout-navigation', require('./components/layout/NavigationComponent').default);
Vue.component('paginate', require('./components/layout/PaginateComponent').default);
Vue.component('blog-posts', require('./components/blog/BlogPostsComponent').default);
Vue.component('blog-post', require('./components/blog/BlogPostComponent').default);
Vue.component('blog-post-show', require('./components/blog/BlogPostShowComponent').default);
Vue.component('blog-create', require('./components/blog/BlogCreateComponent').default);
Vue.component('blog-edit', require('./components/blog/BlogEditComponent').default);
Vue.component('blog-sidebar', require('./components/blog/BlogSidebarComponent').default);
Vue.component('post-rating', require('./components/blog/RatingComponent').default);

Vue.mixin({
    data() {
        return {
            //
        }
    },
    methods: {
        validateIfFormIsFilled(variables) {
            let toreturn = true;
            _.forEach(variables, function (variable) {
                if(!variable || variable === null || variable === '') {
                    console.log('validateEmptyValues failed app.js');
                    toreturn = false;
                }
            });
            return toreturn;
        },
    },
    computed: {

    },
    created() {
    }
});

import Vuex from "vuex";

Vue.use(Vuex);

const app = new Vue({
    el: '#app',
});
