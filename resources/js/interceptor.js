/**
 * Intercept the incoming responses.
 *
 * Handle any unexpected HTTP errors and pop up modals, etc.
 */
window.axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {

    if (error.response === undefined) {
        alert('Unable to find a connection to the server. Is your internet service down?');
        return error;
    }

    switch (error.response.status) {
        case 400:
            alert(error.response.data.message);

        case 400:
        case 401:
        case 402:
            window.location.href = '/blog';
            break;
        case 500:
            alert(error.response.data.message);
            return Promise.reject(error);
            break;

        default:
            return Promise.reject(error);
    }
});
